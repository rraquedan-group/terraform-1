# Starter Terraform Demo

This is a simple Terraform project that deploys an Apache server to Google Cloud. This project does not use Docker and instead focuses on using Terraform to manage the infrastructure. The project includes all the necessary Terraform files to deploy an Apache server and also provides instructions on how to use the project.

## Prerequisites

Before getting started, you'll need to have the following:

- A Google Cloud account
- A GitLab account
- A basic understanding of Terraform

## Getting Started

To get started with this project, follow these steps:

1. Clone the repository to your local machine.

2. Navigate to the project directory

3. Set up your Google Cloud credentials by following the instructions [here](https://console.cloud.google.com/getting-started?pli=1).

4. Create a service account and download the JSON key for your Google Cloud project by following the instructions [here](https://cloud.google.com/iam/docs/keys/create-service-account-key).

5. Convert the JSON key to base64-encoded format by running the following command: base64 <path-to-json-key-file> | pbcopy

6. Add the `TF_VAR_google_key` environment variable to your GitLab project with the base64-encoded contents of the JSON key file.

7. Commit and push your changes to GitLab. The deployment will automatically start.

8. Once the deployment is complete, you should see the IP address of the server in the output. You can then access the Apache server by navigating to that IP address in your web browser.

## Conclusion

This project provides a simple example of how to deploy an Apache server to Google Cloud using Terraform and GitLab CI/CD with environment variables. While this project is basic, it can be used as a starting point for more complex infrastructure deployments.