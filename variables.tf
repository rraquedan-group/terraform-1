variable "google_key" {
  type        = string
  description = "Google Cloud account authentication key"
}

variable "machine_type" {
  type        = string
  default     = "e2-small"
  description = "Compute Engine instance type"
}

variable "document_root" {
  type        = string
  default     = "/var/www/html"
  description = "Document Root"
}
