terraform {
  backend "http" {
  }
}

provider "google" {
  credentials = base64decode(var.google_key)
  project     = ""
  region      = "us-central1"
}

# Create a static external IP address
resource "google_compute_address" "static_ip" {
  name = "terraform-demo-ip"
}

# Set up a web-server
resource "google_compute_instance" "default" {
  name                      = "terraform-gitlab-demo"
  machine_type              = var.machine_type
  zone                      = "us-central1-b"
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.static_ip.address
    }
  }

  metadata_startup_script = <<-EOF
  sudo apt-get update && \
  sudo apt-get install apache2 -y && \
  sudo apt-get install git -y && \
  sudo rm -rf ${var.document_root} && \
  sudo git clone https://gitlab.com/regnard-demos/dashboard-template.git ${var.document_root} && \
  sudo service apache2 restart
  EOF
}

# to allow http traffic
resource "google_compute_firewall" "default" {
  name    = "allow-http-traffic-firewall"
  network = "default"
  allow {
    ports    = ["80"]
    protocol = "tcp"
  }
  source_ranges = ["0.0.0.0/0"]
}
